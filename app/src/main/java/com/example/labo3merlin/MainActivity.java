package com.example.labo3merlin;

import androidx.appcompat.app.AppCompatActivity;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.List;

public class MainActivity extends AppCompatActivity
{

    Button btnShowLocation;
    GPSLocationTracker gps;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final Geocoder geocoder = new Geocoder(this);

        btnShowLocation = findViewById(R.id.btnLocation);
        btnShowLocation.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                gps = new GPSLocationTracker(MainActivity.this);
                if(gps.canGetLocation)
                {
                    double latitude = gps.getLatitude();
                    double longitude = gps.getLongitude();

                    List<Address> addressList = null;
                    try
                    {
                        addressList = geocoder.getFromLocation(latitude,longitude,1);
                    }
                    catch (IOException e)
                    {
                        e.printStackTrace();
                    }

                    String pays = addressList.get(0).getCountryName();
                    String province = addressList.get(0).getAdminArea();
                    String ville = addressList.get(0).getLocality();

                    Toast.makeText(getApplicationContext(),"Votre position est \nLat: "
                                  +latitude+"\nLongitude: "+longitude
                                  +"\npays:"+pays
                                  +"\nCode Province: "+province
                                  +"\nVille: "+ville,Toast.LENGTH_LONG).show();
                }
                else
                {
                    gps.showSettingsAlert();
                }
            }
        });
    }
}